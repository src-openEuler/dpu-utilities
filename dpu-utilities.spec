Name:          dpu-utilities 
Summary:       openEuler dpu utilities
Version:       1.10
Release:       7
License:       GPL-2.0
Source:        https://gitee.com/openeuler/dpu-utilities/repository/archive/v%{version}.tar.gz
ExclusiveOS:   linux
ExclusiveArch: x86_64 aarch64
URL:           https://gitee.com/openeuler/dpu-utilities
BuildRoot:     %{_tmppath}/%{name}-%{version}-root
Conflicts:     %{name} < %{version}-%{release}
Provides:      %{name} = %{version}-%{release}
%define kernel_version %(ver=`rpm -qa|grep kernel-devel`;echo ${ver#*kernel-devel-})
BuildRequires: kernel-devel >= 5.10, gcc, make, json-c-devel, glib2-devel

Patch1: 0001-refactor-syscall-wrapper-for-aarch64-reduce-the-memo.patch
Patch2: 0002-fix-readdir-bug-in-devtmpfs.patch
Patch3: 0003-Change-the-maximum-number-of-qtfs-links-from-16-to-6.patch
Patch4: 0004-add-compile-option-of-EPOLL_PROXY-and-fix-some-probl.patch
Patch5: 0005-resolve-migration-issues.patch
Patch6: 0006-fix-rwlock-cause-schedule-bug-if-pagefault-or-wait-f.patch

%description
This package contains the software utilities on dpu.

%package -n dpuos-imageTailor-config
Summary: dpuos imageTailor configrations
Requires: imageTailor

%description -n dpuos-imageTailor-config
imageTailor configration files for dpuos

%package -n qtfs-client
Summary: Client of qtfs
Requires: json-c, glib2

%description -n qtfs-client
qtfs is a shared file system, this is the client of qtfs.

%package -n qtfs-server
Summary: Server of qtfs
Requires: json-c, glib2

%description -n qtfs-server
qtfs is a shared file system, this is the server of qtfs.

%prep
%autosetup -n %{name}-v%{version} -p1

%build
sed -i "s#KBUILD=.*#KBUILD=/lib/modules/%{kernel_version}/build#" %_builddir/%{name}-v%{version}/qtfs/qtfs/Makefile
sed -i "s#KBUILD=.*#KBUILD=/lib/modules/%{kernel_version}/build#" %_builddir/%{name}-v%{version}/qtfs/qtfs_server/Makefile
cd %_builddir/%{name}-v%{version}/qtfs/qtfs
make
cd %_builddir/%{name}-v%{version}/qtfs/qtfs_server
make
cd %_builddir/%{name}-v%{version}/qtfs/rexec
make
cd %_builddir/%{name}-v%{version}/qtfs/ipc
make
cd %_builddir/%{name}-v%{version}/qtfs/qtinfo
export role=client
make qtcfg
mv qtcfg qtcfg_client
make clean
export role=server
make qtcfg

%install
mkdir -p $RPM_BUILD_ROOT/lib/modules/%{kernel_version}//extra
mkdir -p $RPM_BUILD_ROOT/usr/bin/
mkdir -p $RPM_BUILD_ROOT/usr/lib/
mkdir -p $RPM_BUILD_ROOT/usr/local/bin
mkdir -p $RPM_BUILD_ROOT/etc/qtfs
mkdir -p $RPM_BUILD_ROOT/etc/rexec
install %_builddir/%{name}-v%{version}/qtfs/qtfs/qtfs.ko $RPM_BUILD_ROOT/lib/modules/%{kernel_version}/extra
install %_builddir/%{name}-v%{version}/qtfs/qtfs_server/qtfs_server.ko $RPM_BUILD_ROOT/lib/modules/%{kernel_version}/extra
install -m 0700 %_builddir/%{name}-v%{version}/qtfs/qtfs_server/engine $RPM_BUILD_ROOT/usr/bin/
install -m 0700 %_builddir/%{name}-v%{version}/qtfs/rexec/rexec ${RPM_BUILD_ROOT}/usr/bin/
install -m 0700 %_builddir/%{name}-v%{version}/qtfs/rexec/rexec_server ${RPM_BUILD_ROOT}/usr/bin/
install -m 0700 %_builddir/%{name}-v%{version}/qtfs/ipc/udsproxyd ${RPM_BUILD_ROOT}/usr/bin/
install -m 0700 %_builddir/%{name}-v%{version}/qtfs/ipc/libudsproxy.so ${RPM_BUILD_ROOT}/usr/lib/
install -m 0700 %_builddir/%{name}-v%{version}/qtfs/qtinfo/qtcfg ${RPM_BUILD_ROOT}/usr/bin/
install -m 0700 %_builddir/%{name}-v%{version}/qtfs/qtinfo/qtcfg_client ${RPM_BUILD_ROOT}/usr/local/bin/qtcfg
install -m 0400 %_builddir/%{name}-v%{version}/qtfs/config/rexec/whitelist ${RPM_BUILD_ROOT}/etc/rexec
install -m 0400 %_builddir/%{name}-v%{version}/qtfs/config/qtfs/whitelist ${RPM_BUILD_ROOT}/etc/qtfs
mkdir -p $RPM_BUILD_ROOT/opt/imageTailor
%ifarch x86_64
cp -rf %_builddir/%{name}-v%{version}/dpuos/image_tailor_cfg/x86_64/custom $RPM_BUILD_ROOT/opt/imageTailor
cp -rf %_builddir/%{name}-v%{version}/dpuos/image_tailor_cfg/x86_64/kiwi $RPM_BUILD_ROOT/opt/imageTailor
%endif
%ifarch aarch64
cp -rf %_builddir/%{name}-v%{version}/dpuos/image_tailor_cfg/aarch64/custom $RPM_BUILD_ROOT/opt/imageTailor
cp -rf %_builddir/%{name}-v%{version}/dpuos/image_tailor_cfg/aarch64/kiwi $RPM_BUILD_ROOT/opt/imageTailor
%endif

%clean
rm -rf ${RPM_BUILD_ROOT}

%post -n qtfs-client
if [[ "$1" = "1" || "$1" = "2" ]] ; then  #1: install 2: update
    /sbin/depmod -a > /dev/null 2>&1 || true
fi

%post -n qtfs-server
if [[ "$1" = "1" || "$1" = "2" ]] ; then  #1: install 2: update
    /sbin/depmod -a > /dev/null 2>&1 || true
fi

%postun -n qtfs-client
if [ "$1" = "0" ] ; then  #0: uninstall
    /sbin/depmod -a > /dev/null 2>&1 || true
fi

%postun -n qtfs-server
if [ "$1" = "0" ] ; then  #0: uninstall
    /sbin/depmod -a > /dev/null 2>&1 || true
fi


%files -n qtfs-client
%attr(0644, root, root) /lib/modules/%{kernel_version}/extra/qtfs.ko
%attr(0500, root, root) /usr/bin/rexec_server
%attr(0500, root, root) /usr/bin/rexec
%attr(0500, root, root) /usr/lib/libudsproxy.so
%attr(0500, root, root) /usr/bin/udsproxyd
%attr(0500, root, root) /usr/local/bin/qtcfg
%attr(0400, root, root) /etc/rexec/whitelist

%files -n qtfs-server
%attr(0644, root, root) /lib/modules/%{kernel_version}/extra/qtfs_server.ko
%attr(0500, root, root) /usr/bin/engine
%attr(0500, root, root) /usr/bin/rexec_server
%attr(0500, root, root) /usr/bin/rexec
%attr(0500, root, root) /usr/bin/qtcfg
%attr(0400, root, root) /etc/qtfs/whitelist
%attr(0400, root, root) /etc/rexec/whitelist

%files -n dpuos-imageTailor-config
/opt/imageTailor/custom/*
/opt/imageTailor/kiwi/*

%post -n dpuos-imageTailor-config
sed -i '/# product_name  product_type/a\dpuos           PANGEA        EMBEDDED   DISK     ISO          install_mode=install install_media=CD install_repo=CD selinux=0' /opt/imageTailor/kiwi/eulerkiwi/product.conf
sed -i '/# product cut_conf/a\dpuos      kiwi/minios/cfg_dpuos  yes' /opt/imageTailor/kiwi/eulerkiwi/minios.conf
sed -i '/<repository_rule>/a\dpuos          1           rpm-dir     euler_base' /opt/imageTailor/repos/RepositoryRule.conf

%changelog
* Fri Nov 22 2024 liqiang <liqiang64@huawei.com> 1.10-7
- Fix rwlock caused schedule bug, change to semaphore

* Mon Oct 21 2024 l30022887 <liweiqiang24@huawei.com> 1.10-6
- resolve migration issues

* Tue Sep 10 2024 liqiang <liqiang64@huawei.com> 1.10-5
- Add compile option for epoll proxy thread

* Tue Jul 23 2024 liqiang <liqiang64@huawei.com> 1.10-4
- Change the maximum number of qtfs links from 16 to 64

* Wed Jul 10 2024 liqiang <liqiang64@huawei.com> 1.10-3
- Fix readdir bug in devtmpfs

* Mon Jun 24 2024 liqiang <liqiang64@huawei.com> 1.10-2
- Refactor syscall wrapper for aarch64 reduce the memory

* Fri May 24 2024 liqiang <liqiang64@huawei.com> 1.10-1
- Update to v1.10 for some features and bugfix

* Tue Dec 26 2023 liqiang <liqiang64@huawei.com> 1.6-7
- Update recent bugfixes

* Sat Dec 16 2023 liqiang <liqiang64@huawei.com> 1.6-6
- Update readme description and fix a bug

* Sat Dec 16 2023 liqiang <liqiang64@huawei.com> 1.6-5
- fix event misalignment problem

* Thu Dec 14 2023 liqiang <liqiang64@huawei.com> 1.6-4
- Fix suspend and fd leak of fifo

* Wed Dec 13 2023 liqiang <liqiang64@huawei.com> 1.6-3
- Update some bugfix

* Sat Dec 9 2023 liqiang <liqiang64@huawei.com> 1.6-2
- fix some problem of fifo, resolve problem in libvirt

* Fri Dec 1 2023 Guangxing Deng <dengguangxing@huawei.com> 1.6-1
- Upgrade dpu-utilities version to 1.6

* Thu Nov 23 2023 Guangxing Deng <dengguangxing@huawei.com> 1.5-1
- Upgrade dpu-utilities version to 1.5

* Mon Aug 21 2023 Weifeng Su <suweifeng1@huawei.com> 1.4-3
- Adapt for kernel 6.4

* Mon Jun 12 2023 Weifeng Su <suweifeng1@huawei.com> 1.4-2
- Sync patches from source

* Fri Jun 2 2023 Weifeng Su <suweifeng1@huawei.com> 1.4-1
- Upgrade dpu-utilities version to 1.4

* Tue Mar 21 2023 Weifeng Su <suweifeng1@huawei.com> 1.3-1
- Upgrade dpu-utilities version to 1.3

* Thu Dec 15 2022 YangXin <245051644@qq.com> 1.1-4
- Fix inode sync error between client and server.
* Thu Dec 8 2022 Weifeng Su <suweifeng1@huawei.com> 1.1-3
- Sync patches from master

* Thu Dec 1 2022 Weifeng Su <suweifeng1@huawei.com> 1.1-2
- add path put in xattr set

* Mon Nov 28 2022 Weifeng Su <suweifeng1@huawei.com> 1.1-1
- Upgrade dpu-utilities version to 1.1

* Wed Aug 17 2022 yangxin <245051644@qq.com> 1.0-2
- Split dpu-utilities into three packages.
* Fri Aug 12 2022 yangxin <245051644@qq.com> 1.0-1
- First Spec Version Include qtfs shared filesystem Driver Code
